#include <genesis.h>
#include "resources.h"


s16 offset = 0;
s16 imageOffset = 0;

int main( u16 hard ) {
	
  	VDP_setPalette(PAL0, rkbg_image.palette->data);
  	VDP_setPalette(PAL1, bird_image.palette->data);

	// set scrolling mode.  HSCROLL_PLANE Affects the WHOLE plane
	VDP_setScrollingMode(HSCROLL_PLANE, VSCROLL_PLANE);

	// get our position in VRAM.
	int ind = TILE_USERINDEX;

	VDP_loadTileSet(bird_image.tileset, ind, DMA);

	VDP_drawImageEx(BG_A, &bird_image, TILE_ATTR_FULL(PAL1, 0, 0, 0, ind), 0, 0, 0, 1);
	// Load the plane tiles into VRAM at our position

	int indexB = ind + bird_image.tileset->numTile;
	VDP_loadTileSet(rkbg_image.tileset, indexB, DMA);

	// put out the image
	VDP_setTileMapEx(BG_B, rkbg_image.tilemap, TILE_ATTR_FULL(PAL0, FALSE, FALSE, FALSE, indexB),
									 0,			// Plane X destination
									 0,			// plane Y destination
									 0, 		// Region X start position
									 0,			// Region Y start position
									 40,		// width  (went with 64 becasue default width is 64.  Viewable screen is 40)
									 64,		// height
									 CPU);

	while (TRUE)
	{
		// tile in memory
		offset += 1;
		if (offset > 511)
			offset = 0;

		// tile from image
		imageOffset += 1;
		if (imageOffset > 1023)
			imageOffset = 0; // bg image is 1280 pixels wide

		if (offset % 8 == 0)
		{
			s16 dstCol = (offset + 504) / 8;
			if (dstCol > 63)
			{
				dstCol -= 64; // wrap to zero
			}

			s16 srcCol = (imageOffset + 512) / 8;
			if (srcCol > 127)
			{
				srcCol -= 128; // wrap to zero
			}

			KLog_S2("dstCol: ", dstCol, "srcCol: ", srcCol);
			VDP_setTileMapEx(BG_B, rkbg_image.tilemap, TILE_ATTR_FULL(PAL0, FALSE, FALSE, FALSE, indexB),
											 0, // Plane X destination
											 dstCol,			 // plane Y destination
											 0, // Region X start position
											 srcCol,			 // Region Y start position
											 40,			 // width
											 1,		 // height
											 CPU);
		}

		VDP_setVerticalScroll(BG_B, offset);

		// let SGDK do its thing
		SYS_doVBlankProcess();
	}
	return 0;
}
