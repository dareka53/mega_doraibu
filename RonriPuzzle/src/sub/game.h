#include <genesis.h>
#include <resources.h>
#include <string.h>

void updateScoreDisplay();

void moveBall();

void myJoyHandler_game(u16 joy, u16 changed, u16 state);

void positionPlayer();

void showText(char s[]);

void startGame();

void endGame();

void gameMain();
