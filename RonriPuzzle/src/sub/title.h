#include <genesis.h>

void updateCursorPosition();
void moveUp();
void moveDown();
void pickStart();
void pickOptions();
void pickExit();
void select(u16 Option);
void joyEventHandler_title(u16 joy, u16 changed, u16 state);
void titleMain();

int isModeFlag();