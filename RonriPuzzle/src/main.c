#include <genesis.h>
#include <resources.h>
#include "sub/title.h"
#include "sub/game.h"

enum GAME_STATE
{
    STATE_MENU,
    STATE_PLAY
};

enum GAME_STATE currentState;

void basicInit()
{
    JOY_init();
    currentState = STATE_MENU;
}

void processStateMenu()
{
    titleMain();
}

void processStatePlay()
{
    VDP_clearTextArea(8, 8, 20, 20);
    gameMain();
}

int main()
{
    basicInit();
    int modeState = 0;
    
    while(1)
    {
        modeState = isModeFlag();
        if(modeState == 0)
        {
            processStateMenu();
        }
        else if(modeState == 1)
        {
            processStatePlay();
        }
    }
    return (0);
}